let trainers = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: [ "Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn : [ "May", "Max"],
		kanto:  ["Brock", "Misty"],
	},
	talk: function(){
		console.log("Pickachu! I choose you!")
	}


}
console.log(trainers);
console.log("Result of dot notation: ");
console.log(trainers.name);
console.log("Result of square brackets: ");
console.log(trainers['pokemon']);
console.log("Result of talk method: ");
trainers.talk();





function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		let remainingHealth = target.health - this.attack;
		
		console.log(this.name + ' tackle ' + target.name);
		console.log(target.name + "'s health is now reduce to " + Number(target.health - this.attack));
		
		if (target.health <= 0){
			target.faint()
		}
	}
	this.faint = function(){
	console.log(this.name + "fainted.");
	}
}

let Pickachu = new Pokemon("Pikachu", 12);
let Geodude = new Pokemon("Geodude", 8);
let Mewtwo = new Pokemon("Mewtwo", 100);

console.log(Pickachu);
console.log(Geodude);
console.log(Mewtwo);

Geodude.tackle(Pickachu);
Mewtwo.tackle(Geodude);